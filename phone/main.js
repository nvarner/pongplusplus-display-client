let phoneMovementData = {
    "code": "",
    "id": "",
    "acceleration": [0, 0, 0],
    "orientation": [0, 0, 0],
    "orientation_rate": [0, 0, 0]
};

function handleOrientation(event) {
    phoneMovementData["orientation"] = [
        event.alpha,
        event.beta,
        event.gamma
    ];
}

function getMotion(event) {
    phoneMovementData["acceleration"] = [
        event.acceleration.x,
        event.acceleration.y,
        event.acceleration.z
    ];

    phoneMovementData["orientation_rate"] = [
        event.rotationRate.alpha,
        event.rotationRate.beta,
        event.rotationRate.gamma
    ];
}

function getRotationRates(event) {
    phoneMovementData["orientation_rate"] = [
        event.alpha,
        event.beta,
        event.gamma
    ];
}

window.addEventListener('deviceorientation', handleOrientation);
window.addEventListener('devicemotion', getMotion);

const socket = io.connect("10.72.75.9:5000");

socket.on("connect", function() {
    // alert("Websocket connected!");
    // alert("Congrats.");
    // alert("You did it!");
});

socket.on("controller_connected", function(msg) {
    // alert("The controller has connected.");
    // alert("You may commence playing the stuff.")
});

socket.on("start_game", () => {
    setInterval(() => {
        socket.emit("controller_update", phoneMovementData);
    }, 100);
});

document.querySelector("form").addEventListener("submit", (e) => {
    e.preventDefault();
    const code = document.querySelector("#code").value;
    phoneMovementData["code"] = code;

    const id = `${Math.random()}`;
    phoneMovementData["id"] = id;

    socket.emit("connect_controller", {
        "code": code,
        "id": id
    });
});