var sock = io.connect('10.72.75.9:5000');

var lasttime = 0;

var user_x_accel = 0, user_y_accel = 0, user_z_accel = 0;
var opponent_x_accel = 0, opponent_y_accel = 0, opponent_z_accel = 0;

let uRot = {
    x: 0,
    y: 0,
    z: 0
};

sock.on('connect', function() {
    sock.emit('create');
});

sock.on('ping', function (_) {
    sock.emit('ping', 'pong!');
});

sock.on('game_code', function (dat) {
    alert(dat.code);
    alert('start game');
    sock.emit('start_game', {"code": dat.code});
});

let uPaddleV = {
    x: 0,
    y: 0,
    z: 0
};
let oPaddleV = {
    x: 0,
    y: 0,
    z: 0
};

setTimeout(function() {
    sock.on('controller_update', function (dat) {
        console.log(dat);
        user_x_accel = dat[0].accel.x;
        user_y_accel = dat[0].accel.y;
        user_z_accel = dat[0].accel.z;

        // opponent_x_accel = dat[1].accel.x;
        // opponent_y_accel = dat[1].accel.y;
        // opponent_z_accel = dat[1].accel.z;

        uRot.x = dat[0].orientation.y;
        uRot.y = dat[0].orientation.x - 1;
        uRot.z = -dat[0].orientation.z;
    });
}, 2000);

setInterval(
    function() {
        var coords = user_paddle.getAttribute('position');

        if (user_x_accel > 0.5) {
            coords.x = 5;
        } else if (user_x_accel < -0.5) {
            coords.x = -5;
        } else {
            coords.x = 0;
        }

        coords.x = 0;
        coords.y = 0;
        coords.z = 0;

        // uPaddleV.x += user_x_accel / 100000;
        // uPaddleV.y += user_y_accel / 100000;
        // uPaddleV.z += user_z_accel / 100000;
        
        // cords.x += uPaddleV.x;
        // cords.y += uPaddleV.y;
        // cords.z += uPaddleV.z;

        // coords.x *= 0.5;

        user_paddle.setAttribute('position', coords);
        user_paddle.setAttribute("rotation", uRot);
    },
    1
);

setInterval(
    function() {
        var coords = opponent_paddle.getAttribute('position');
        // cords.x = opponent_x_accel / 100;
        // cords.y = opponent_y_accel / 100;
        // cords.z = opponent_z_accel / 100;

        coords.x = 0;
        coords.y = 100;
        coords.z = 100;
        opponent_paddle.setAttribute('position', coords);
    },
    1
);
