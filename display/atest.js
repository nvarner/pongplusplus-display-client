var user_paddle = document.getElementById('user-paddle');
var opponent_paddle = document.getElementById('opponent-paddle');

window.onload = function() {
    var modifier = -1;
    var scene = document.querySelector('a-scene');
    var cylinder = document.createElement('a-cylinder');
    cylinder.setAttribute('color', '#FF9500');
    cylinder.setAttribute('height', '2');
    cylinder.setAttribute('radius', '0.75');
    cylinder.setAttribute('position', '3 1 0');
    scene.appendChild(cylinder);
    var t = 0;
    function render() {
      t += 0.01;
      requestAnimationFrame(render);
      cylinder.setAttribute('position', '3 '+(Math.sin(t*2)+1)+' 0');
    }
    render();

    /*document.body.addEventListener('keyup', function(evt) {
        if(evt.key.startsWith('Arrow')) {
            var cords = user_paddle.getAttribute('position');
            if (evt.key == "ArrowUp") cords.y++;
            if (evt.key == "ArrowDown") cords.y--;
            if (evt.key == "ArrowLeft") cords.x--;
            if (evt.key == "ArrowRight") cords.x++;
            console.log('x: ' + cords.x);
            user_paddle.setAttribute('position', cords);
        } else {
            var cords = opponent_paddle.getAttribute('position');
            if (evt.key == "W") cords.y++;
            if (evt.key == "A") cords.y--;
            if (evt.key == "S") cords.x--;
            if (evt.key == "D") cords.x++;
            console.log('x: ' + cords.x);
            user_paddle.setAttribute('position', cords);
        }
    });*/

    /*setInterval(function () {
        var cords = user_paddle.getAttribute('position');
        cords.x += 0.25 * modifier;
        if(cords.x > 15) modifier = -1;
        if(cords.x < -15) modifier = 1;
        user_paddle.setAttribute('position', cords);
    }, 20);*/
};
