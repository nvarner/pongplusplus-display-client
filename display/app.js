const world = new WHS.World({
  autoresize: "window",
  stats: 'fps', // Statistics tool for developers.

  rendering: {
    background: {
      color: 0x162129
    }
  },

  gravity: [0, -100, 0], // Physic gravity.

  camera: {
    position: {
      z: 50
    }
  }
});

const sphere = new WHS.Sphere({ // Create sphere comonent.
  geometry: {
    radius: 3,
    widthSegments: 32,
    heightSegments: 32
  },

  mass: 10, // Mass of physics object.

  material: {
    color: 0xF2F2F2,
    kind: 'basic'
  },

  position: [0, 10, 0]
});

sphere.addTo(world); // Add sphere to world.