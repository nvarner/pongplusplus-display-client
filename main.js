const socket = io.connect("http://10.72.75.9:5000");

let code = "";

socket.on("connect", () => {
    socket.emit("create");
});

socket.on("game_code", function displayCode(data) {
    const codeInput = document.querySelector("#code");
    codeInput.value = data["code"];
    code = data["code"];
});

socket.on("controller_connected", (data) => {
    alert(`Connected: ${data["id"]}`);
});

setTimeout(() => {
    socket.on("controller_update", (data) => {
        console.log(data);
    });
}, 2000);

document.querySelector("#start").addEventListener("click", () => {
    socket.emit("start_game", {
        "code": code
    });
});
